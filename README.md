## Maildev + PHP

### Run it

```bash
$ docker-compose up -d
$ docker-compose exec php
```

### Testing

Either do it in CLI:
```bash
/var/www/app# php mail.test.php
```

Or, go to [mail.test.php](http://localhost:8080/mail.test.php)