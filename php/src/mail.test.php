<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_STRICT);

require_once "vendor/autoload.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//PHPMailer Object
$mail = new PHPMailer(true); //Argument true in constructor enables exceptions

$mail->IsSMTP();
$mail->Host = "mail";
$mail->Port = 25;
$mail->SMTPSecure = false;
$mail->SMTPAutoTLS = false;

// OPtional
$mail->SMTPAuth = false;
// $mail->Username = 'smtp_username';
// $mail->Password = 'smtp_password';

//From email address and name
$mail->From = "from@yourdomain.com";
$mail->FromName = "Full Name";

//To address and name
$mail->addAddress("recepient1@example.com", "Recepient Name");
$mail->addAddress("recepient1@example.com"); //Recipient name is optional

//Address to which recipient will reply
$mail->addReplyTo("reply@yourdomain.com", "Reply");

//CC and BCC
$mail->addCC("cc@example.com");
$mail->addBCC("bcc@example.com");

//Send HTML or Plain Text email
$mail->isHTML(true);

$mail->Subject = "Subject Text";
$mail->Body = "<i>Mail body in HTML</i>";
$mail->AltBody = "This is the plain text version of the email content";

try {
  $mail->send();
  echo "Message has been sent successfully";
} catch (Exception $e) {
  echo "Mailer Error: " . $mail->ErrorInfo . PHP_EOL;
}